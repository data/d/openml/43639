# OpenML dataset: Amazon-Stock-Data(Oct-2019---July-2020)LATEST

https://www.openml.org/d/43639

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This Data set Contains the values of Stock of Amazon which dates between 1st October,2019 to 14 July,2020
Content
The Data set contains 7 different columns that includes Date, The opening value of the stock, the closing value of stock, volume and a few other things necessary to make predictions.
Acknowledgements
This data was scraped from Yahoo finance and you can also use their API to access the data.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43639) of an [OpenML dataset](https://www.openml.org/d/43639). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43639/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43639/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43639/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

